<?php
/*
This is where we include all the function files. Plus some core WP functions
are added at the bottom.
*/

// ************* INCLUDE FUNCTION FILES ***************

// CORE FLEXDEV FUNCTIONS
require_once('inc/flexdev.php'); // if you remove this the Internet blows up, just sayin
require_once('inc/fdt-enqueues.php');
require_once('inc/fdt-images.php');

// FOR CREATING CPT / CUSTOM TAXONOMY
require_once('inc/custom-post-type.php');  // you can disable this line if not using CPTs
require_once('inc/custom-taxonomy.php');  // you can disable this line if not using Custom Taxonomy

// CUSTOMIZE THE WORDPRESS ADMIN
require_once('inc/admin.php');

// CUSTOM FUNCTIONS FOR EACH WEBSITE
require_once('inc/fdt-custom.php');

// ANY SHORTCODES YOU ARE USING
require_once('inc/shortcodes.php');  // disable if not using any shortocdes


/* DON'T DELETE THIS CLOSING TAG */ ?>
