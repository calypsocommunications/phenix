<footer class="Strip  PageFooter  Strip--paddingReset" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
  <div class="SectionContainer">

    <nav class="SecondaryMenuWrap" role="navigation">
      <?php fdt_nav_menu( 'footer-nav', 'SecondaryMenu' ) // Adjust using Menus in Wordpress Admin ?>
    </nav>

    <div class="FooterLogo">
      <svg class="icon icon-logo-footer"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-logo-footer"></use></svg>
    </div>

    <div class="FooterAddress">
      <p>18 N. Main Street, suite 202<br />Concord, NH 03301<br />603.225.7737</p>
      <div class="FooterSocial">
        <a target="_blank" href="#"><svg class="icon icon-icon-facebook"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-icon-facebook"></use></svg></a>
        <a target="_blank" href="#"><svg class="icon icon-icon-twitter"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-icon-twitter"></use></svg></a>
        <a target="_blank" href="#"><svg class="icon icon-icon-mail"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-icon-mail"></use></svg></a>
      </div>
    </div>


  </div> <!-- /container-->
</footer> <!-- /footer -->

<?php get_template_part( 'parts/modal-menu' ); ?>

<!-- all js scripts are loaded in lib/fdt-enqueues.php -->
<?php wp_footer(); ?>

</body>

</html> <!-- end page. what a ride! -->
