# FlexDev: The Flexible Developer Theme

The future of web development is finally here. The painful old days of catering to the lowest common browser denominator is over. And with that, no longer the long waits for new tech that is supported in the latest browsers. And so, this is where FlexDev comes in. It is meant to fully embrace the new era of web development. It's also intended to enable rapid clean web development. Full credit has to go to Eddie Machado who created the [Bones Theme](http://themble.com/bones), which did provide the inspiration and original basis of this theme.

********************

## *CHANGELOG*

## v1.1.0

* remove Neat 1.0 stuff

## v1.0.4

* minor tweaks and improvements

## v1.0.3

* add flex safe container wrapper (no clearfix)
  * eventually will become SectionContainer once neat grid is removed; whereby clearfix can be added to a container when needed
* add flex columns helper
* move modal menu into a template part

## v1.0.2

* sane z-index management with Sass function for layers
* update colour variables
* various minor edits/fixes

## v1.0.1

* pos-rel utility class
* misc post launch clean up and fixes
* add object-fit polyfill

## v1.0.0

* official first release
* sass comment style clean up, unifying comment styles

## v1.0.0rc1

* clean up formatting
* fix visualblocks path bug

## v0.2.1
* update Slick carousel script
* migrate helpers to their own project
* text overlay image helper
* vertical centering utility classes

## v0.2.0
* additional class renaming
* helper clean up and revamp begins...
* flex powered columns helper mixin

## v0.1.0
* major class renaming to SUIT CSS convention
* template file structure revamp

## v0.0.4
* default mobile navigation setup (using modified animatedModal.js)
* menu and logo absolute positioning
* major reorganization of theme backend, key/most-used files separated out

## v0.0.3
* grid variable revamp / grid mixin update: variable names should all match up now
* same variables for grid used in breakpoint mixin

## v0.0.2
* total revamp of sass folder structure
* change WP editor font to Merriweather; tweak editor styles
* clean up readme file

## v0.0.1
* initial commit...time to rip apart the old UBNG theme and FlexDev it up!


## KEY PARTS & TIPS
* CSS generally follows these [best practices](http://cssguidelin.es/), except for naming convention
* why to [avoid "content semantic" classes](http://mrmrs.io/writing/2016/03/24/scalable-css/); instead use class names that are:
  * [highly re-useable](http://cssguidelin.es/#naming)
  * [content-independent](http://nicolasgallagher.com/about-html-semantics-front-end-architecture/)
  * KEY: a component usually shouldn't have to live in a certain place to look a certain way
* component building philosophy: [SUIT CSS Components](https://github.com/suitcss/suit/blob/master/doc/components.md)
* naming convention: [SUIT CSS Naming](https://github.com/suitcss/suit/blob/master/doc/naming-conventions.md)
* modal box using [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/) included in core theme files (js/scss folders, scripts.js)
* favicons setup through WP admin or use [Favicon Generator](http://realfavicongenerator.net/) for optimal setup


## TO DO
* nothing...


## KNOWN BUGS
* no bugs here. this thing is perfect. ish.
