<?php get_header(); ?>

<div class="Strip  Hero" >
  <div class="SectionContainer">
    <div class="HeroText u-verticalCenterTransform">
      <h1>This Is Main Street</h1>
      <p>
        By building a foundation for growing businesses and artistic endeavors, Phenix Block will help transform Main Street into a destination that serves the community and inspires its residents.
      </p>
      <div class="LinkButton LinkButton--green ">
        <a href="#">Watch Video</a>
      </div>
    </div> <!-- /HeroText -->
  </div> <!-- /SectionContainer -->
  <a href="#Section1" class="DownLink"><svg class="icon icon-arrow-down"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-arrow-down"></use></svg></a>
</div>

<div class="EntryContent">
  <div class="Strip City Strip--blackbg" id="Section1">
    <div class="CityPhoto"><img src="<?php bloginfo('template_url') ?>/assets/img/guy.png" alt="" /></div>
    <div class="SectionContainer">
      <div class="QuoteSlider">

        <svg class="icon icon-open-quote"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-open-quote"></use></svg>

        <div class="QuoteSlider-slides">
          <div class="QuoteSlide">
            <h3>Concord lorem ipsum dolor sit amet. Proin gravida nibh vel velit auctor aliquet. dolor sit amet proin gravida nibh lorem ipsum.</h3>
            <p class="QuoteSlide-cite">
              Firstname Lastname<br /><span>Occupation</span>
            </p>
          </div>
          <div class="QuoteSlide">
            <h3>Concord lorem ipsum dolor sit amet. Proin gravida nibh vel velit auctor aliquet. dolor sit amet proin gravida nibh lorem ipsum.</h3>
            <p class="QuoteSlide-cite">
            Firstname Lastname<br /><span>Occupation</span>
          </p>
          </div>
        </div>

          <svg class="icon icon-end-quote"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-end-quote"></use></svg>

      </div> <!-- /QuoteSlider -->
    </div> <!-- /SectionContainer -->
  </div>

  <div class="Strip Spaces Strip--grayLightest" id="Section2">
    <div class="SectionContainer ">
      <div class="">
        <h2>The Spaces</h2>
        <p class="Spaces-description">
          <strong>Main Street is more than address</strong> — it is a system of spaces designed to connect and create the ideal place to work and play.  With a prime location in the heart of New Hampshire's capital city, you're only steps away from diverse restaurants, entertainment venues, and specialty retail shops.
        </p>
        <div class="OfficeTypes">
          <span class="DoubleLine-up"></span>
          <ul>
            <li>Sunlit office suites</li><li>city views</li><li>crafted to suit </li>
          </ul>
          <span class="DoubleLine-down"></span>
        </div>

        <div class="SpacesSlider">

          <div class="slide">
            <img src="<?php bloginfo('template_url') ?>/assets/img/slidetest.jpg" alt="" />
            <h4>Type Of Office Name</h4>
          </div>
          <div class="slide">
            <img src="<?php bloginfo('template_url') ?>/assets/img/slidetest.jpg" alt="" />
          <h4>Type Of Office Name</h4>
          </div>
          <div class="DoubleLine-Full">

          </div>
        </div>
      </div>
    </div> <!-- /SectionContainer -->
  </div>

  <div class="Strip  History Strip--white" id="Section2b">
    <div class="GrayStripe"></div>
    <div class="SectionContainer">
      <div class="HistorySlider">
        <?php if( have_rows('history_slider') ): ?>
          <div class="HistorySlider-bg">

          </div>
          <?php while( have_rows('history_slider') ): the_row(); ?>

            <div class="HistorySlider-slide">
              <?php
              if (get_sub_field('history_image')) {
                $imageArray = get_sub_field('history_image'); // Array returned by Advanced Custom Fields
                $imageAlt = $imageArray['alt'];
                $imageTitle = $imageArray['title'];
                $imageURL = $imageArray['url']; // Grab the full size version URL
                $imageCropURL = $imageArray['sizes']['crop-320']; // (sizes: thumbnail, medium, large or 'crop-size-name' as set in functions)
                // now show the image
                echo '<img src="' . $imageCropURL . '" alt="' . $imageAlt .'" title="' . $imageTitle . '" />';
              }
              ?>
            </div>

          <?php endwhile; ?>
        <?php endif; ?>
      </div>
    </div>
    <div class="SectionContainer">
      <div class="HistoryCTA">
        <h2>Historically Entertaining</h2>
        <p>It's more than just four walls, a soundboard, and a stage; it's an event venue that dates back to the 19th century. From Abraham Lincoln to wrestling matches, Phenix Hall has been the site of some paramount moments in history. Now is your chance to weave together this historic past and the future, right in the center of Concord.</p>
      </div> <!-- /class_name -->
    </div> <!-- /SectionContainer -->
  </div>

  <div class="Strip  Experience Strip--gray" id="Section3">
    <div class="SectionContainer">
      <div class="Experience-wrap u-verticalCenterTransform">
        <div class="Experience-content">
          <h4><span class="Hugetext">Since 1964,</span><br />Ciborowski Associates has worked to create a better Main Street experience. </h4>
          <p>Jacob Ciborowski morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. This vision is supported by simultaneously balancing Concord's rich history with developing downtown into a space to be enjoyed by residents and visitors for years to come. </p>
          <span class="DoublLine  DoublLine--left"></span>
          <svg class="icon icon-logo-icon"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-logo-icon"></use></svg>
          <span class="DoublLine  DoublLine--right"></span>

        </div> <!-- /History-content -->
      </div>
    </div> <!-- /SectionContainer -->
  </div>

  <div class="Strip  Ready Strip--green" id="Section4">
    <div class="SectionContainer">
      <div class="u-verticalCenterTransform">
        <?php gravity_form(1, false, false, false, '', true, 12); ?>
      </div> <!-- /class_name -->
    </div> <!-- /SectionContainer -->
  </div>
</div>

<?php get_footer(); ?>
