<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <?php // force Internet Explorer to use the latest rendering engine available ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <?php // mobile meta (hooray!) ?>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Playfair+Display|Reem+Kufi" rel="stylesheet">

  <!-- wordpress head functions -->
  <?php wp_head(); ?>
  <!-- end of wordpress head -->

  <?php // drop Google Analytics here ?>
  <?php // end Analytics ?>

</head>

<body <?php body_class(pretty_body_class()); ?> itemscope itemtype="http://schema.org/WebPage">
<!--[if lt IE 11]><p class="browserwarning">Your browser is <strong>very old.</strong> Please <a href="http://browsehappy.com/">upgrade to a different browser</a> to properly browse this web site.</p><![endif]-->

  <header class="Strip  PageHeader Strip--paddingReset" role="banner" itemscope itemtype="http://schema.org/WPHeader">

    <div class="SectionContainer">

      <div class="SiteLogo" itemscope itemtype="http://schema.org/Organization">
        <a href="/" rel="nofollow">
          <svg class="icon icon-logo"><use xlink:href="<?php bloginfo('template_url') ?>/assets/img/symbol-defs.svg#icon-logo"></use></svg>
        </a>
      </div>

      <nav class="MainMenuWrap" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
        <ul class="MainMenu">
          <li><a href="#Section1">The City</a></li>
          <li><a href="#Section2">The Spaces</a></li>
          <li><a href="#Section3">The Experience</a></li>
          <li><a href="#Section4">The Company</a></li>
        </ul>
        <?php // fdt_nav_menu( 'main-nav', 'MainMenu' ); // Adjust using Menus in WordPress Admin ?>
      </nav>

      <div class="LinkButton JoinLink">
        <a href="#Section4">Join Waitlist</a>
      </div>

      <a href="#ModalNav" id="ModalMenuButton" class="ModalMenuButton">OPEN MENU</a>

    </div> <!-- /SectionContainer -->

  </header> <!-- /PageHeader -->
