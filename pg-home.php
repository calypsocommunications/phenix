<?php
/*
Template Name: Home Page
*/
?>

<?php get_header(); ?>

<div class="Strip Strip-fullHero">
  <div class="SectionContainer">

  </div> <!-- /SectionContainer -->
</div>

  <div class="Strip">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <section class="EntryContent  cf">
          <h1 class="MainTitle"><?php the_title(); ?></h1>
          <?php the_content(); ?>
        </section> <!-- /EntryContent -->

      <?php endwhile; endif; // END main loop (if/while) ?>

      <?php // IF USING PARTS ->  get_template_part( 'parts/name-of-part' ); ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>
