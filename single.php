<?php get_header(); ?>

  <div class="Strip">
    <main class="SectionContainer" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/WebPageElement">
      <div class="PrimaryContent">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          <article <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

            <header class="ArticleHeader">

              <h1 itemprop="headline"><?php the_title(); ?></h1>

              <div class="EntryMeta">
                <?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'boilerplate_author_bio_avatar_size', 40 ) ); ?>
                <span class="EntryMeta-author" itemprop="author" itemscope itemptype="http://schema.org/Person">By: <?php echo get_the_author(); ?></span>
                <span>Date: <time datetime="<?php the_time('Y-m-d'); ?>" itemprop="datePublished"><?php the_time('d.m.Y'); ?></time></span>
                <span>Categories: <?php the_category(', '); ?></span>
              </div> <!-- /EntryMeta -->

            </header> <!-- /ArticleHeader -->

            <section class="EntryContent  BlogContent  cf" itemprop="articleBody">
              <?php the_content(); ?>
            </section> <!-- /EntryContent -->

            <footer class="ArticleFooter">

              <?php the_tags('<p class="tags"><span class="tags-title">Tags:</span> ', ', ', '</p>'); ?>

            </footer> <!-- /article footer -->

            <?php comments_template(); // comments should go inside the article element ?>

          </article> <!-- /article -->

        <?php endwhile; else : ?>

          <article class="PostNotFound">
            <header class="ArticleHeader">
              <h1><?php _e("Oops, Post Not Found!", "flexdev"); ?></h1>
            </header>
            <section class="EntryContent">
              <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "flexdev"); ?></p>
            </section>
            <footer class="ArticleFooter">
              <p><?php _e("This is the error message in the single.php template.", "flexdev"); ?></p>
            </footer>
          </article>

        <?php endif; ?>

      </div> <!-- /PrimaryContent -->

      <?php get_sidebar(); // sidebar ?>

    </main>
  </div> <!-- /Strip-->

<?php get_footer(); ?>
