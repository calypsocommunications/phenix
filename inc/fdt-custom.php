<?php
/**
 * Any small custom functions for this project? If yes, drop them here
 */


// *************** DETETERMINE IF A PAGE IS WITHIN A TREE ********************
function is_tree( $pid ) {      // $pid = The ID of the page we're looking for pages underneath
  global $post;          // load details about this page
  if ( is_page($pid) )
    return true;        // we're at the page or at a sub page
  $anc = get_post_ancestors( $post->ID );
  foreach ( $anc as $ancestor ) {
    if( is_page() && $ancestor == $pid ) {
      return true;
    }
  }
  return false;          // we aren't at the page, and the page is not an ancestor
}



// ************** CHECK IF PAGE HAS PARENT ****************
function page_has_parent() {
  global $post;

  $children = get_pages('child_of='.$post->ID);
  if( count( $children ) > 0 ) {
      $parent = true;
  }

  return $parent;
}



// ************ CUSTOM TITLE LENGTH *********************
/* usage: <?php echo title_crop(55); ?> */
function title_crop($count) {
  $title = get_the_title();
  if (strlen($title) > $count) {
    $title = substr($title, 0, $count) . '...';
  }
  return $title;
}

// Hide label option added to Gravity Forms
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );



?>
