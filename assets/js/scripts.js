/**
 * FlexDev Theme Scripts File **************
 * This file contains any js scripts you want added to the theme. Instead of calling it in the header or throwing
 * it inside wp_head() this file will be called automatically in the footer so as not to slow the page load.
 */



/* JQUERY VIEWPORT RE-SIZING? --> SEE HELPERS/RESPONSIVE MISC */


jQuery(document).ready(function ($) {
// *********************** START CUSTOM SCRIPTS *******************************

  // init magnificPopup setup for images *****************
  // $('.magnific').magnificPopup({
  //   type: 'image',
  //   closeOnContentClick: true,
  //   closeBtnInside: false,
  //   fixedBgPos: true,
  //   mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
  //   image: {
  //     verticalFit: true
  //   },
  //   zoom: {
  //     enabled: true,
  //     duration: 300 // don't foget to change the duration also in CSS; ZOOM DOES NOT WORK ON TEXT LINK
  //   }
  // });
  //

  $('.HistorySlider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    arrows: false,
    centerPadding: '0%',
    slide: '.HistorySlider-slide'
 });

 $('.QuoteSlider-slides').slick({
  infinite: true,
  slidesToScroll: 1,
  centerMode: false,
  arrows: false

});

$(".SpacesSlider").slick({
  slide: '.slide'
});

  //Scrollto
  //From main nav
  $('.PageHeader a[href^="#"]').live("click", function(e){
    // Prevent the jump and the #hash from appearing on the address bar
      e.preventDefault();
      // Scroll the window, stop any previous animation, stop on user manual scroll
      // Check https://github.com/flesler/jquery.scrollTo for more customizability
      $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:true, offset:-120});
   });

   //From Join Link
   $('.JoinLink a[href^="#"]').live("click", function(e){
  // Prevent the jump and the #hash from appearing on the address bar
    e.preventDefault();
    // Scroll the window, stop any previous animation, stop on user manual scroll
    // Check https://github.com/flesler/jquery.scrollTo for more customizability
    $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:true, offset:-120});
 });

 //From Join Link
$('.DownLink[href^="#"]').live("click", function(e){
// Prevent the jump and the #hash from appearing on the address bar
 e.preventDefault();
 // Scroll the window, stop any previous animation, stop on user manual scroll
 // Check https://github.com/flesler/jquery.scrollTo for more customizability
 $(window).stop(true).scrollTo(this.hash, {duration:1000, interrupt:true, offset:-120});
});


   //Sticky menu
  $(window).scroll(function() {
  if ($(this).scrollTop() > 1){
      $('.PageHeader').addClass("sticky");
      // $('#PopdownMenu').addClass("ShowPopdown");
    }
    else{
      $('.PageHeader').removeClass("sticky");
    }
  });



  // modal menu init
  var modal_menu = jQuery("#ModalMenuButton").animatedModal({
    modalTarget: 'ModalNav',
    animatedIn: 'slideInDown',
    animatedOut: 'slideOutUp',
    animationDuration: '0.50s',
    color: '#ACDCAC'
  });
  // close modal menu if esc key is used
  jQuery(document).keyup(function(ev){
    if(ev.keyCode == 27) {
      modal_menu.close();
    }
  });



  // objectfit polyfill
  $(function () { objectFitImages('.u-polyFit'); });




// *********************** END CUSTOM SCRIPTS *********************************

}); /* end of DOM ready scripts */
